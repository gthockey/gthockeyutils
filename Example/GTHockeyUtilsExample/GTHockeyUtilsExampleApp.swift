//
//  GTHockeyUtilsExampleApp.swift
//  GTHockeyUtilsExample
//
//  Created by Caleb Rudnicki on 1/24/22.
//

import SwiftUI

@main
struct GTHockeyUtilsExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
