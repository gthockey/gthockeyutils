//
//  ImageCarouselView.swift
//  
//
//  Created by Caleb Rudnicki on 3/6/22.
//

import SwiftUI
import GTHockeyUtils

struct ImageCarouselView: View {
    var body: some View {
        ImageCarousel(imageUrls: [URL(string: "https://prod.gthockey.com/media/shop/White3_2.jpg")!,
                                  URL(string: "https://prod.gthockey.com/media/shop/White1.jpg")!,
                                  URL(string: "https://prod.gthockey.com/media/shop/20181108_153742658_iOS.jpg")!,
                                  URL(string: "https://prod.gthockey.com/media/shop/20181108_153746573_iOS.jpg")!,
                                  URL(string: "https://prod.gthockey.com/media/shop/20181108_153734603_iOS_a6uvKuu.jpg")!,
                                  URL(string: "https://prod.gthockey.com/media/shop/Rebirth_Sizing_Chart_vOmhwYl.JPG")!])
    }
}

//struct ImageCarouselView_Previews: PreviewProvider {
//    static var previews: some View {
//        ImageCarouselView()
//    }
//}
