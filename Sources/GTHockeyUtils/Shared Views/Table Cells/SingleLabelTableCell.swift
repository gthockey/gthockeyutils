//
//  SingleLabelTableCell.swift
//  
//
//  Created by Caleb Rudnicki on 1/27/22.
//

import SwiftUI

public struct SingleLabelTableCell: View {
    
    private let label: String
    
    public init(label: String) {
        self.label = label
    }
    
    public var body: some View {
        Text(label)
            .font(.normalText)
            .foregroundColor(.text)
            .padding([.top, .bottom], 20)
            .listRowBackground(Color.background)
        
    }
}

struct SingleLabelTableCell_Previews: PreviewProvider {
    static var previews: some View {
        SingleLabelTableCell(label: "Preview Label")
    }
}
