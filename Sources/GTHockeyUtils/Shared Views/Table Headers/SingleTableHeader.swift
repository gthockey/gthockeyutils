//
//  SingleTableHeader.swift
//  
//
//  Created by Caleb Rudnicki on 1/30/22.
//

import SwiftUI

public struct SingleTableHeader: View {
    
    private var label: String
    
    public init(_ label: String) {
        self.label = label
    }
    
    public var body: some View {
        if #available(iOS 15.0, *) {
            CustomText(label, font: .mediumText)
                .padding(.bottom, 10)
                .padding(.top, 20)
                .listRowBackground(Color.background)
                .listRowSeparator(.hidden)
        } else {
            // Fallback on earlier versions
            CustomText(label, font: .mediumText)
                .padding(.bottom, 10)
                .padding(.top, 20)
                .listRowBackground(Color.background)
        }
    }
}

struct SingleTableHeader_Previews: PreviewProvider {
    static var previews: some View {
        SingleTableHeader("Testing")
    }
}
