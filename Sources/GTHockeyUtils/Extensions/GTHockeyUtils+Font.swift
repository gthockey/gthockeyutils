//
//  GTHockeyUtils+Font.swift
//  
//
//  Created by Caleb Rudnicki on 1/25/22.
//

import SwiftUI

public extension Font {

    /// Font: Oswald-Bold
    /// Size: 80
    static var heading1: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Bold", size: 80)
    }

    /// Font: Oswald-Bold
    /// Size: 60
    static var heading2: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Bold", size: 60)
    }
    
    /// Font: Oswald-Bold
    /// Size: 30
    static var heading3: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Bold", size: 30)
    }
    
    /// Font: Oswald-Bold
    /// Size: 24
    static var heading4: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Bold", size: 24)
    }
    
    /// Font: Oswald-Regular
    /// Size: 24
    static var largeText: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Regular", size: 24)
    }
    
    /// Font: Oswald-Regular
    /// Size: 20
    static var mediumText: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Regular", size: 20)
    }
    
    /// Font: Oswald-Regular
    /// Size: 18
    static var standardCaption: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Regular", size: 18)
    }
    
    /// Font: Oswald-Regular
    /// Size: 16
    static var normalText: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Regular", size: 16)
    }
    
    /// Font: Oswald-Light
    /// Size: 16
    static var lightText: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Light", size: 16)
    }
    
    /// Font: Oswald-Regular
    /// Size: 12
    static var smallText: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Regular", size: 12)
    }
    
    /// Font: Oswald-Regular
    /// Size: 12
    static var smallCaption: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-Regular", size: 12)
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 24
    static var largeTextSemibold: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-SemiBold", size: 24)
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 20
    static var mediumTextSemibold: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-SemiBold", size: 20)
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 18
    static var captionSemibold: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-SemiBold", size: 18)
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 16
    static var normalTextSemibold: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-SemiBold", size: 16)
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 12
    static var smallTextSemibold: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-SemiBold", size: 12)
    }
    
    /// Font: Oswald-SemiBold
    /// Size: 12
    static var smallCaptionSemibold: Font {
        if !FontLoader.fontsHaveBeenLoaded { FontLoader.blast(bundle: .module) }
        return Font.custom("Oswald-SemiBold", size: 12)
    }

}
