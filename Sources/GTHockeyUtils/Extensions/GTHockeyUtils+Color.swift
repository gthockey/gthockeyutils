//
//  GTHockeyUtils+Color.swift
//  
//
//  Created by Caleb Rudnicki on 1/24/22.
//

import Foundation
import SwiftUI

@available(macOS 10.15, *)
public extension Color {
    
    /// Color to be used as the background color for the app
    static var background = Color("Background", bundle: .module)
    
    /// Color to be used as a section background color
    static var secondaryBackground = Color("SecondaryBackground", bundle: .module)
    
    /// Color to be used for all text elements
    static var text = Color("Text", bundle: .module)
    
    /// Color replicating the team gold color
    static var gold = Color("Gold", bundle: .module)
    
    /// Color replicating the team navy color
    static var navy = Color("Navy", bundle: .module)
    
    /// Color to be used as an accent color
    static var powder = Color("Powder", bundle: .module)
    
    /// Color to be used for all destructive elements
    static var destructive = Color("Destructive", bundle: .module)
    
}
